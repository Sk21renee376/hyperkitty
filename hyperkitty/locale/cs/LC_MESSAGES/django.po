# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-20 11:22-0800\n"
"PO-Revision-Date: 2022-01-28 20:41+0000\n"
"Last-Translator: Sebastian Machat <sebi@outlook.com>\n"
"Language-Team: Czech <https://hosted.weblate.org/projects/gnu-mailman/"
"hyperkitty/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 4.11-dev\n"

#: forms.py:53
msgid "Add a tag..."
msgstr "Přidat tag..."

#: forms.py:55
msgid "Add"
msgstr "Přidat"

#: forms.py:56
msgid "use commas to add multiple tags"
msgstr "jako oddělovač vkládaných tagů použijte čárku"

#: forms.py:64
msgid "Attach a file"
msgstr "Připojit soubor"

#: forms.py:65
msgid "Attach another file"
msgstr "Připojit další přílohu"

#: forms.py:66
msgid "Remove this file"
msgstr "Odebrat přílohu"

#: templates/hyperkitty/404.html:28
msgid "Error 404"
msgstr "Chyba 404"

#: templates/hyperkitty/404.html:30 templates/hyperkitty/500.html:31
msgid "Oh No!"
msgstr "Ale ne!"

#: templates/hyperkitty/404.html:32
msgid "I can't find this page."
msgstr "Nemůžu najít tuto stránku."

#: templates/hyperkitty/404.html:33 templates/hyperkitty/500.html:34
msgid "Go back home"
msgstr "Zpět domů"

#: templates/hyperkitty/500.html:29
msgid "Error 500"
msgstr "Chyba 500"

#: templates/hyperkitty/500.html:33
msgid "Sorry, but the requested page is unavailable due to a server hiccup."
msgstr ""
"Omlouváme se, ale kvůli chybce na serveru je požadovaná stránka nedostupná."

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
#, fuzzy
msgid "started"
msgstr "začal"

#: templates/hyperkitty/ajax/reattach_suggest.html:7
#: templates/hyperkitty/reattach.html:25
msgid "last active:"
msgstr "naposledy aktivní:"

#: templates/hyperkitty/ajax/reattach_suggest.html:8
msgid "see this thread"
msgstr "zobrazit toto vlákno"

#: templates/hyperkitty/ajax/reattach_suggest.html:12
msgid "(no suggestions)"
msgstr "(žádné návrhy)"

#: templates/hyperkitty/ajax/temp_message.html:12
msgid "Sent just now, not yet distributed"
msgstr "Čerstvě odeslán, ale zatím nerozeslán"

#: templates/hyperkitty/api.html:5
msgid "REST API"
msgstr "REST API"

#: templates/hyperkitty/api.html:7
msgid ""
"HyperKitty comes with a small REST API allowing you to programatically "
"retrieve emails and information."
msgstr ""
"HyperKitty obsahuje jednoduché REST API, které Vám umožňuje získávat emaily "
"a informace vlastním kódem."

#: templates/hyperkitty/api.html:10
msgid "Formats"
msgstr "Formáty"

#: templates/hyperkitty/api.html:12
msgid ""
"This REST API can return the information into several formats.  The default "
"format is html to allow human readibility."
msgstr ""
"Toto REST API umí vrátit informace v různých formátech. Výchozím formátem "
"je, pro srozumitelnost lidem, html."

#: templates/hyperkitty/api.html:14
msgid ""
"To change the format, just add <em>?format=&lt;FORMAT&gt;</em> to the URL."
msgstr ""
"Pro změnu formátu jednoduše do URL přidejte <em>?format=&lt;FORMAT&gt;</em>."

#: templates/hyperkitty/api.html:16
msgid "The list of available formats is:"
msgstr "Seznam dostupných formátů:"

#: templates/hyperkitty/api.html:20
msgid "Plain text"
msgstr "Plain text"

#: templates/hyperkitty/api.html:26
msgid "List of mailing-lists"
msgstr "Seznam mailing-listů"

#: templates/hyperkitty/api.html:27 templates/hyperkitty/api.html:33
#: templates/hyperkitty/api.html:39 templates/hyperkitty/api.html:45
#: templates/hyperkitty/api.html:51
msgid "Endpoint:"
msgstr "Endpoint:"

#: templates/hyperkitty/api.html:29
msgid ""
"Using this address you will be able to retrieve the information known about "
"all the mailing lists."
msgstr ""
"Pomocí této adresy budete moci získat dostupné informace o všech mailing-"
"listech."

#: templates/hyperkitty/api.html:32
msgid "Threads in a mailing list"
msgstr "Vlákna v mailing-listu"

#: templates/hyperkitty/api.html:35
msgid ""
"Using this address you will be able to retrieve information about all the "
"threads on the specified mailing list."
msgstr ""
"Pomocí této adresy budete moci získat dostupné informace o všech vláknech v "
"daném mailing-listu."

#: templates/hyperkitty/api.html:38
msgid "Emails in a thread"
msgstr "Emaily ve vlákně"

#: templates/hyperkitty/api.html:41
msgid ""
"Using this address you will be able to retrieve the list of emails in a "
"mailing list thread."
msgstr ""
"Pomocí této adresy budete moci získat seznam emailů ve vlákně mailing-listu."

#: templates/hyperkitty/api.html:44
msgid "An email in a mailing list"
msgstr "Email v mailing-listu"

#: templates/hyperkitty/api.html:47
msgid ""
"Using this address you will be able to retrieve the information known about "
"a specific email on the specified mailing list."
msgstr ""
"Pomocí této adresy budete moci získat dostupné informace o konkrétním emailu "
"v daném mailing-listu."

#: templates/hyperkitty/api.html:50
msgid "Tags"
msgstr "Tagy"

#: templates/hyperkitty/api.html:53
msgid "Using this address you will be able to retrieve the list of tags."
msgstr "Pomocí této adresy budete moci získat seznam tagů."

#: templates/hyperkitty/base.html:57 templates/hyperkitty/base.html:112
msgid "Account"
msgstr "Účet"

#: templates/hyperkitty/base.html:62 templates/hyperkitty/base.html:117
msgid "Mailman settings"
msgstr "Nastavení Mailmanu"

#: templates/hyperkitty/base.html:67 templates/hyperkitty/base.html:122
#: templates/hyperkitty/user_profile/base.html:17
msgid "Posting activity"
msgstr "Příspěvková aktivita"

#: templates/hyperkitty/base.html:72 templates/hyperkitty/base.html:127
msgid "Logout"
msgstr "Odhlásit se"

#: templates/hyperkitty/base.html:78 templates/hyperkitty/base.html:134
msgid "Sign In"
msgstr "Přihlásit se"

#: templates/hyperkitty/base.html:82 templates/hyperkitty/base.html:138
msgid "Sign Up"
msgstr "Zaregistrovat se"

#: templates/hyperkitty/base.html:91
msgid "Search this list"
msgstr "Prohledat tento list"

#: templates/hyperkitty/base.html:91
msgid "Search all lists"
msgstr "Prohledat všechny listy"

#: templates/hyperkitty/base.html:149
msgid "Manage this list"
msgstr "Spravovat tento list"

#: templates/hyperkitty/base.html:154
msgid "Manage lists"
msgstr "Spravovat listy"

#: templates/hyperkitty/base.html:192
msgid "Keyboard Shortcuts"
msgstr "Klávesové zkratky"

#: templates/hyperkitty/base.html:195
msgid "Thread View"
msgstr "Zobrazení vláken"

#: templates/hyperkitty/base.html:197
msgid "Next unread message"
msgstr "Následující nepřečtená zpráva"

#: templates/hyperkitty/base.html:198
msgid "Previous unread message"
msgstr "Předchozí nepřečtená zpráva"

#: templates/hyperkitty/base.html:199
msgid "Jump to all threads"
msgstr "Přejít na všechna vlákna"

#: templates/hyperkitty/base.html:200
msgid "Jump to MailingList overview"
msgstr "Přejít na přehled Mailing-listu"

#: templates/hyperkitty/base.html:214
msgid "Powered by"
msgstr "Běží na"

#: templates/hyperkitty/base.html:214
msgid "version"
msgstr "verze"

#: templates/hyperkitty/errors/notimplemented.html:7
msgid "Not implemented yet"
msgstr "Zatím nebylo vyvinuto"

#: templates/hyperkitty/errors/notimplemented.html:12
msgid "Not implemented"
msgstr "Nevyvinuto"

#: templates/hyperkitty/errors/notimplemented.html:14
msgid "This feature has not been implemented yet, sorry."
msgstr "Omlouváme se, tato funkce zatím nebyla vyvinuta."

#: templates/hyperkitty/errors/private.html:7
msgid "Error: private list"
msgstr "Chyba: soukromý list"

#: templates/hyperkitty/errors/private.html:19
msgid ""
"This mailing list is private. You must be subscribed to view the archives."
msgstr ""
"Tento mailing-list je soukromý. Pro zobrazení archivu se musíte přihlásit."

#: templates/hyperkitty/fragments/like_form.html:16
msgid "You like it (cancel)"
msgstr "Toto se Vám líbí (zrušit)"

#: templates/hyperkitty/fragments/like_form.html:24
msgid "You dislike it (cancel)"
msgstr "Toto se Vám nelíbí (zrušit)"

#: templates/hyperkitty/fragments/like_form.html:27
#: templates/hyperkitty/fragments/like_form.html:31
msgid "You must be logged-in to vote."
msgstr "Pro hlasování se musíte přihlásit."

#: templates/hyperkitty/fragments/month_list.html:6
msgid "Threads by"
msgstr "Vlákna podle"

#: templates/hyperkitty/fragments/month_list.html:6
msgid " month"
msgstr " měsíc"

#: templates/hyperkitty/fragments/overview_threads.html:11
msgid "New messages in this thread"
msgstr "Nové zprávy v tomto vlákně"

#: templates/hyperkitty/fragments/overview_threads.html:36
#: templates/hyperkitty/fragments/thread_left_nav.html:19
#: templates/hyperkitty/overview.html:78
msgid "All Threads"
msgstr "Všechna vlákna"

#: templates/hyperkitty/fragments/overview_top_posters.html:15
msgid "See the profile"
msgstr "Zobrazit profil"

#: templates/hyperkitty/fragments/overview_top_posters.html:21
msgid "posts"
msgstr "příspěvky"

#: templates/hyperkitty/fragments/overview_top_posters.html:26
msgid "No posters this month (yet)."
msgstr "Tento měsíc zatím nikdo nepřispěl."

#: templates/hyperkitty/fragments/send_as.html:5
msgid "This message will be sent as:"
msgstr "Tato zpráva bude odeslána jako:"

#: templates/hyperkitty/fragments/send_as.html:6
msgid "Change sender"
msgstr "Změna odesílatele"

#: templates/hyperkitty/fragments/send_as.html:16
msgid "Link another address"
msgstr "Propojit jinou adresu"

#: templates/hyperkitty/fragments/send_as.html:20
msgid ""
"If you aren't a current list member, sending this message will subscribe you."
msgstr ""
"Pokud nejste v současnosti členem mail-listu, odeslání této zprávy Vás k "
"odběru automaticky přihlásí."

#: templates/hyperkitty/fragments/thread_left_nav.html:12
msgid "List overview"
msgstr "Přehled listů"

#: templates/hyperkitty/fragments/thread_left_nav.html:29 views/message.py:74
#: views/mlist.py:102 views/thread.py:168
msgid "Download"
msgstr "Stáhnout"

#: templates/hyperkitty/fragments/thread_left_nav.html:32
msgid "Past 30 days"
msgstr "Uplynulých 30 dní"

#: templates/hyperkitty/fragments/thread_left_nav.html:33
msgid "This month"
msgstr "Tento měsíc"

#: templates/hyperkitty/fragments/thread_left_nav.html:36
msgid "Entire archive"
msgstr "Celý archiv"

#: templates/hyperkitty/index.html:9 templates/hyperkitty/index.html:63
msgid "Available lists"
msgstr "Dostupné listy"

#: templates/hyperkitty/index.html:22 templates/hyperkitty/index.html:27
#: templates/hyperkitty/index.html:72
msgid "Most popular"
msgstr "Nejoblíbenější"

#: templates/hyperkitty/index.html:26
msgid "Sort by number of recent participants"
msgstr "Seřadit podle počtu nedávných účastníků"

#: templates/hyperkitty/index.html:32 templates/hyperkitty/index.html:37
#: templates/hyperkitty/index.html:75
msgid "Most active"
msgstr "Nejaktivnější"

#: templates/hyperkitty/index.html:36
msgid "Sort by number of recent discussions"
msgstr "Seřadit dle počtu nedávných diskuzí"

#: templates/hyperkitty/index.html:42 templates/hyperkitty/index.html:47
#: templates/hyperkitty/index.html:78
msgid "By name"
msgstr "Dle jména"

#: templates/hyperkitty/index.html:46
msgid "Sort alphabetically"
msgstr "Abecedně seřadit"

#: templates/hyperkitty/index.html:52 templates/hyperkitty/index.html:57
#: templates/hyperkitty/index.html:81
msgid "Newest"
msgstr "Nejnovější"

#: templates/hyperkitty/index.html:56
msgid "Sort by list creation date"
msgstr "Seřadit podle data vytvoření"

#: templates/hyperkitty/index.html:68
msgid "Sort by"
msgstr "Seřadit podle"

#: templates/hyperkitty/index.html:91
msgid "Hide inactive"
msgstr "Schovat neaktivní"

#: templates/hyperkitty/index.html:92
msgid "Hide private"
msgstr "Skrýt soukromé"

#: templates/hyperkitty/index.html:99
msgid "Find list"
msgstr "Vyhledat list"

#: templates/hyperkitty/index.html:123 templates/hyperkitty/index.html:191
#: templates/hyperkitty/user_profile/last_views.html:34
#: templates/hyperkitty/user_profile/last_views.html:73
msgid "new"
msgstr "nový"

#: templates/hyperkitty/index.html:135 templates/hyperkitty/index.html:202
msgid "private"
msgstr "soukromé"

#: templates/hyperkitty/index.html:137 templates/hyperkitty/index.html:204
msgid "inactive"
msgstr "neaktivní"

#: templates/hyperkitty/index.html:143 templates/hyperkitty/index.html:229
#: templates/hyperkitty/overview.html:94 templates/hyperkitty/overview.html:111
#: templates/hyperkitty/overview.html:181
#: templates/hyperkitty/overview.html:188
#: templates/hyperkitty/overview.html:195
#: templates/hyperkitty/overview.html:204
#: templates/hyperkitty/overview.html:212 templates/hyperkitty/reattach.html:39
#: templates/hyperkitty/thread.html:111
msgid "Loading..."
msgstr "Načítání..."

#: templates/hyperkitty/index.html:160 templates/hyperkitty/index.html:237
msgid "No archived list yet."
msgstr "Žádné listy zatím nebyly archivovány."

#: templates/hyperkitty/index.html:172
#: templates/hyperkitty/user_profile/favorites.html:40
#: templates/hyperkitty/user_profile/last_views.html:45
#: templates/hyperkitty/user_profile/profile.html:15
#: templates/hyperkitty/user_profile/subscriptions.html:41
#: templates/hyperkitty/user_profile/votes.html:46
msgid "List"
msgstr "List"

#: templates/hyperkitty/index.html:173
msgid "Description"
msgstr "Popis"

#: templates/hyperkitty/index.html:174
msgid "Activity in the past 30 days"
msgstr "Aktivita v posledních 30 dnech"

#: templates/hyperkitty/index.html:218 templates/hyperkitty/overview.html:103
#: templates/hyperkitty/thread_list.html:40
#: templates/hyperkitty/threads/right_col.html:97
#: templates/hyperkitty/threads/summary_thread_large.html:54
msgid "participants"
msgstr "účastníci"

#: templates/hyperkitty/index.html:223 templates/hyperkitty/overview.html:104
#: templates/hyperkitty/thread_list.html:45
msgid "discussions"
msgstr "diskuze"

#: templates/hyperkitty/list_delete.html:7
msgid "Delete MailingList"
msgstr "Smazat mailing-list"

#: templates/hyperkitty/list_delete.html:20
msgid "Delete Mailing List"
msgstr "Smazat mailing-list"

#: templates/hyperkitty/list_delete.html:26
msgid ""
"will be deleted along with all the threads and messages. Do you want to "
"continue?"
msgstr "bude smazán i se všemi vlákny a zprávami. Chcete pokračovat?"

#: templates/hyperkitty/list_delete.html:33
#: templates/hyperkitty/message_delete.html:44
msgid "Delete"
msgstr "Smazat"

#: templates/hyperkitty/list_delete.html:34
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
msgid "or"
msgstr "nebo"

#: templates/hyperkitty/list_delete.html:36
#: templates/hyperkitty/message_delete.html:45
#: templates/hyperkitty/message_new.html:53
#: templates/hyperkitty/messages/message.html:146
#: templates/hyperkitty/user_profile/votes.html:36
#: templates/hyperkitty/user_profile/votes.html:74
msgid "cancel"
msgstr "storno"

#: templates/hyperkitty/message.html:22
msgid "thread"
msgstr "vlákno"

#: templates/hyperkitty/message_delete.html:7
#: templates/hyperkitty/message_delete.html:20
msgid "Delete message(s)"
msgstr "Smazat zprávu/zprávy"

#: templates/hyperkitty/message_delete.html:25
#, python-format
msgid ""
"\n"
"        %(count)s message(s) will be deleted. Do you want to continue?\n"
"        "
msgstr ""
"\n"
"        %(count)s zpráv bude smazáno. Chcete pokračovat?\n"
"        "

#: templates/hyperkitty/message_new.html:8
#: templates/hyperkitty/message_new.html:21
msgid "Create a new thread"
msgstr "Vytvořit nové vlákno"

#: templates/hyperkitty/message_new.html:22
#: templates/hyperkitty/user_posts.html:22
msgid "in"
msgstr "v"

#: templates/hyperkitty/message_new.html:52
#: templates/hyperkitty/messages/message.html:145
msgid "Send"
msgstr "Odeslat"

#: templates/hyperkitty/messages/message.html:18
#, python-format
msgid "See the profile for %(name)s"
msgstr "Zobrazit profil uživatele %(name)s"

#: templates/hyperkitty/messages/message.html:28
msgid "Unread"
msgstr "Nepřečteno"

#: templates/hyperkitty/messages/message.html:45
msgid "Sender's time:"
msgstr "Čas odesílatele:"

#: templates/hyperkitty/messages/message.html:51
msgid "New subject:"
msgstr "Nový předmět:"

#: templates/hyperkitty/messages/message.html:61
msgid "Attachments:"
msgstr "Přílohy:"

#: templates/hyperkitty/messages/message.html:76
msgid "Display in fixed font"
msgstr "Zobrazit ve fontu s fixní šířkou"

#: templates/hyperkitty/messages/message.html:79
msgid "Permalink for this message"
msgstr "Permalink k této zprávě"

#: templates/hyperkitty/messages/message.html:90
#: templates/hyperkitty/messages/message.html:96
msgid "Reply"
msgstr "Odpovědět"

#: templates/hyperkitty/messages/message.html:93
msgid "Sign in to reply online"
msgstr "Pro online odpověď se přihlašte"

#: templates/hyperkitty/messages/message.html:105
#, python-format
msgid ""
"\n"
"                %(email.attachments.count)s attachment\n"
"                "
msgid_plural ""
"\n"
"                %(email.attachments.count)s attachments\n"
"                "
msgstr[0] ""
"\n"
"                %(email.attachments.count)s příloha\n"
"                "
msgstr[1] ""
"\n"
"                %(email.attachments.count)s přílohy\n"
"                "
msgstr[2] ""
"\n"
"                %(email.attachments.count)s přílohy\n"
"                "

#: templates/hyperkitty/messages/message.html:131
msgid "Quote"
msgstr "Citace"

#: templates/hyperkitty/messages/message.html:132
msgid "Create new thread"
msgstr "Vytvořit nové vlákno"

#: templates/hyperkitty/messages/message.html:135
msgid "Use email software"
msgstr "Použít emailového klienta"

#: templates/hyperkitty/messages/right_col.html:11
msgid "Back to the thread"
msgstr "Zpět do vlákna"

#: templates/hyperkitty/messages/right_col.html:18
msgid "Back to the list"
msgstr "Zpět do listu"

#: templates/hyperkitty/messages/right_col.html:27
msgid "Delete this message"
msgstr "Smazat tuto zprávu"

#: templates/hyperkitty/messages/summary_message.html:23
#, python-format
msgid ""
"\n"
"                                by %(name)s\n"
"                            "
msgstr ""
"\n"
"                                z %(name)s\n"
"                            "

#: templates/hyperkitty/overview.html:38
msgid "Home"
msgstr "Domů"

#: templates/hyperkitty/overview.html:41 templates/hyperkitty/thread.html:78
msgid "Stats"
msgstr "Statistiky"

#: templates/hyperkitty/overview.html:44
msgid "Threads"
msgstr "Vlákna"

#: templates/hyperkitty/overview.html:50 templates/hyperkitty/overview.html:61
#: templates/hyperkitty/thread_list.html:48
msgid "You must be logged-in to create a thread."
msgstr "Pro vytvoření vlákna se musíte přihlásit."

#: templates/hyperkitty/overview.html:63
#: templates/hyperkitty/thread_list.html:52
msgid ""
"<span class=\"d-none d-md-inline\">Start a n</span><span class=\"d-md-none"
"\">N</span>ew thread"
msgstr ""
"<span class=\"d-none d-md-inline\">Zahájit</span><span class=\"d-md-none"
"\">N</span>ové vlákno"

#: templates/hyperkitty/overview.html:75
msgid ""
"<span class=\"d-none d-md-inline\">Manage s</span><span class=\"d-md-none"
"\">S</span>ubscription"
msgstr ""
"<span class=\"d-none d-md-inline\">Spravovat s</span><span class=\"d-md-none"
"\">O</span>dběr"

#: templates/hyperkitty/overview.html:81
msgid "Delete Archive"
msgstr "Smazat archiv"

#: templates/hyperkitty/overview.html:91
msgid "Activity Summary"
msgstr "Souhrn aktivity"

#: templates/hyperkitty/overview.html:93
msgid "Post volume over the past <strong>30</strong> days."
msgstr "Počet zpráv v posledních <strong>30</strong> dnech."

#: templates/hyperkitty/overview.html:98
msgid "The following statistics are from"
msgstr "Následující statistiky jsou od"

#: templates/hyperkitty/overview.html:99
msgid "In"
msgstr "V"

#: templates/hyperkitty/overview.html:100
msgid "the past <strong>30</strong> days:"
msgstr "posledních <strong>30</strong> dnech:"

#: templates/hyperkitty/overview.html:109
msgid "Most active posters"
msgstr "Nejaktivnější přispěvatelé"

#: templates/hyperkitty/overview.html:118
msgid "Prominent posters"
msgstr "Významní přispěvatelé"

#: templates/hyperkitty/overview.html:133
msgid "kudos"
msgstr "kudos"

#: templates/hyperkitty/overview.html:152
msgid "Recent"
msgstr "Nedávné"

#: templates/hyperkitty/overview.html:156
msgid "Most Active"
msgstr "Nejaktivnější"

#: templates/hyperkitty/overview.html:160
msgid "Most Popular"
msgstr "Nejoblíbenější"

#: templates/hyperkitty/overview.html:166
#: templates/hyperkitty/user_profile/base.html:22
msgid "Favorites"
msgstr "Oblíbené"

#: templates/hyperkitty/overview.html:170
msgid "Posted"
msgstr "Odesláno"

#: templates/hyperkitty/overview.html:179
msgid "Recently active discussions"
msgstr "Diskuze s nedávnou aktivitou"

#: templates/hyperkitty/overview.html:186
msgid "Most popular discussions"
msgstr "Nejoblíbenější diskuze"

#: templates/hyperkitty/overview.html:193
msgid "Most active discussions"
msgstr "Nejaktivnější diskuze"

#: templates/hyperkitty/overview.html:200
msgid "Discussions You've Flagged"
msgstr "Diskuze které jste si označili"

#: templates/hyperkitty/overview.html:208
msgid "Discussions You've Posted to"
msgstr "Diskuze do kterých jste přispěli"

#: templates/hyperkitty/reattach.html:9
msgid "Reattach a thread"
msgstr "Napojit vlákno"

#: templates/hyperkitty/reattach.html:20
msgid "Re-attach a thread to another"
msgstr "Přepojit vlákno k jinému"

#: templates/hyperkitty/reattach.html:22
msgid "Thread to re-attach:"
msgstr "Vlákno k přepojení:"

#: templates/hyperkitty/reattach.html:29
msgid "Re-attach it to:"
msgstr "Přepojit jej k:"

#: templates/hyperkitty/reattach.html:31
msgid "Search for the parent thread"
msgstr "Vyhledat nadřazené vlákno"

#: templates/hyperkitty/reattach.html:32
msgid "Search"
msgstr "Vyhledat"

#: templates/hyperkitty/reattach.html:44
msgid "this thread ID:"
msgstr "ID tohoto vlákna:"

#: templates/hyperkitty/reattach.html:50
msgid "Do it"
msgstr "Provést"

#: templates/hyperkitty/reattach.html:50
msgid "(there's no undoing!), or"
msgstr "(není to možné vrátit), nebo"

#: templates/hyperkitty/reattach.html:52
msgid "go back to the thread"
msgstr "vrátit se do vlákna"

#: templates/hyperkitty/search_results.html:8
msgid "Search results for"
msgstr "Výsledky hledání po"

#: templates/hyperkitty/search_results.html:30
msgid "search results"
msgstr "výsledky hledání"

#: templates/hyperkitty/search_results.html:32
msgid "Search results"
msgstr "Výsledky hledání"

#: templates/hyperkitty/search_results.html:34
msgid "for query"
msgstr "pro dotaz"

#: templates/hyperkitty/search_results.html:44
#: templates/hyperkitty/user_posts.html:34
msgid "messages"
msgstr "zprávy"

#: templates/hyperkitty/search_results.html:57
msgid "sort by score"
msgstr "seřadit dle skóre"

#: templates/hyperkitty/search_results.html:60
msgid "sort by latest first"
msgstr "seřadit od nejnovějších"

#: templates/hyperkitty/search_results.html:63
msgid "sort by earliest first"
msgstr "seřadit od nejstarších"

#: templates/hyperkitty/search_results.html:84
msgid "Sorry no email could be found for this query."
msgstr "Na tento dotaz nebyl nalezen žádný email."

#: templates/hyperkitty/search_results.html:87
msgid "Sorry but your query looks empty."
msgstr "Váš dotaz je, bohužel, prázdný."

#: templates/hyperkitty/search_results.html:88
msgid "these are not the messages you are looking for"
msgstr "toto nejsou zprávy které jste hledali"

#: templates/hyperkitty/thread.html:30
msgid "newer"
msgstr "novější"

#: templates/hyperkitty/thread.html:44
msgid "older"
msgstr "starší"

#: templates/hyperkitty/thread.html:72
msgid "First Post"
msgstr "První příspěvek"

#: templates/hyperkitty/thread.html:75
#: templates/hyperkitty/user_profile/favorites.html:45
#: templates/hyperkitty/user_profile/last_views.html:50
msgid "Replies"
msgstr "Odpovědi"

#: templates/hyperkitty/thread.html:97
msgid "Show replies by thread"
msgstr "Zobrazit odpovědi podle vlákna"

#: templates/hyperkitty/thread.html:100
msgid "Show replies by date"
msgstr "Zobrazit odpovědi podle data"

#: templates/hyperkitty/thread_list.html:60
msgid "Sorry no email threads could be found"
msgstr "Žádná emailová vlákna nebyla, bohužel, nalezena"

#: templates/hyperkitty/threads/category.html:7
msgid "Click to edit"
msgstr "Pro úpravu klikněte"

#: templates/hyperkitty/threads/category.html:9
msgid "You must be logged-in to edit."
msgstr "Pro úpravy musíte být přihlášený."

#: templates/hyperkitty/threads/category.html:15
msgid "no category"
msgstr "žádné kategorie"

#: templates/hyperkitty/threads/right_col.html:12
msgid "Age (days ago)"
msgstr ""

#: templates/hyperkitty/threads/right_col.html:18
#, fuzzy
#| msgid "Last activity"
msgid "Last active (days ago)"
msgstr "Poslední aktivita"

#: templates/hyperkitty/threads/right_col.html:40
#, python-format
msgid "%(num_comments)s comments"
msgstr "%(num_comments)s komentářů"

#: templates/hyperkitty/threads/right_col.html:44
#, fuzzy, python-format
#| msgid "%(thread.participants_count)s participants"
msgid "%(participants_count)s participants"
msgstr "%(thread.participants_count)s účastníků"

#: templates/hyperkitty/threads/right_col.html:49
#, python-format
msgid "%(unread_count)s unread <span class=\"hidden-sm\">messages</span>"
msgstr "%(unread_count)s nepřečtených <span class=\"hidden-sm\">zpráv</span>"

#: templates/hyperkitty/threads/right_col.html:59
msgid "You must be logged-in to have favorites."
msgstr "Pro použití funkce oblíbených se musíte nejdřív přihlásit."

#: templates/hyperkitty/threads/right_col.html:60
msgid "Add to favorites"
msgstr "Přidat do oblíbených"

#: templates/hyperkitty/threads/right_col.html:62
msgid "Remove from favorites"
msgstr "Odebrat z oblíbených"

#: templates/hyperkitty/threads/right_col.html:71
msgid "Reattach this thread"
msgstr "Znovupřipojit toto vlákno"

#: templates/hyperkitty/threads/right_col.html:75
msgid "Delete this thread"
msgstr "Smazat toto vlákno"

#: templates/hyperkitty/threads/right_col.html:113
msgid "Unreads:"
msgstr "Nepřečtených:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "Go to:"
msgstr "Přejít na:"

#: templates/hyperkitty/threads/right_col.html:115
msgid "next"
msgstr "následující"

#: templates/hyperkitty/threads/right_col.html:116
msgid "prev"
msgstr "předcházející"

#: templates/hyperkitty/threads/summary_thread_large.html:21
#: templates/hyperkitty/threads/summary_thread_large.html:23
msgid "Favorite"
msgstr "Oblíbené"

#: templates/hyperkitty/threads/summary_thread_large.html:38
msgid "Most recent thread activity"
msgstr "Nejčerstvější aktivita vlákna"

#: templates/hyperkitty/threads/summary_thread_large.html:59
msgid "comments"
msgstr "komentáře"

#: templates/hyperkitty/threads/tags.html:3
msgid "tags"
msgstr "tagy"

#: templates/hyperkitty/threads/tags.html:9
msgid "Search for tag"
msgstr "Vyhledat tag"

#: templates/hyperkitty/threads/tags.html:15
msgid "Remove"
msgstr "Odstranit"

#: templates/hyperkitty/user_posts.html:8
#: templates/hyperkitty/user_posts.html:21
#: templates/hyperkitty/user_posts.html:25
msgid "Messages by"
msgstr "Zprávy podle"

#: templates/hyperkitty/user_posts.html:38
#, python-format
msgid "Back to %(fullname)s's profile"
msgstr "Zpět na profil %(fullname)s"

#: templates/hyperkitty/user_posts.html:48
msgid "Sorry no email could be found by this user."
msgstr "Žádný email od tohoto uživatele nebyl, bohužel, nalezen."

#: templates/hyperkitty/user_profile/base.html:5
#: templates/hyperkitty/user_profile/base.html:12
msgid "User posting activity"
msgstr "Kolik příspěvků uživatele odeslal"

#: templates/hyperkitty/user_profile/base.html:12
#: templates/hyperkitty/user_public_profile.html:7
#: templates/hyperkitty/user_public_profile.html:14
msgid "for"
msgstr "pro"

#: templates/hyperkitty/user_profile/base.html:26
msgid "Threads you have read"
msgstr "Vlákna které jste přečetli"

#: templates/hyperkitty/user_profile/base.html:30
#: templates/hyperkitty/user_profile/profile.html:18
#: templates/hyperkitty/user_profile/subscriptions.html:45
msgid "Votes"
msgstr "Hlasy"

#: templates/hyperkitty/user_profile/base.html:34
msgid "Subscriptions"
msgstr "Odběry"

#: templates/hyperkitty/user_profile/favorites.html:24
#: templates/hyperkitty/user_profile/last_views.html:27
#: templates/hyperkitty/user_profile/votes.html:23
msgid "Original author:"
msgstr "Původní autor:"

#: templates/hyperkitty/user_profile/favorites.html:26
#: templates/hyperkitty/user_profile/last_views.html:29
#: templates/hyperkitty/user_profile/votes.html:25
msgid "Started on:"
msgstr "Zahájeno:"

#: templates/hyperkitty/user_profile/favorites.html:28
#: templates/hyperkitty/user_profile/last_views.html:31
msgid "Last activity:"
msgstr "Poslední aktivita:"

#: templates/hyperkitty/user_profile/favorites.html:30
#: templates/hyperkitty/user_profile/last_views.html:33
msgid "Replies:"
msgstr "Odpovědi:"

#: templates/hyperkitty/user_profile/favorites.html:41
#: templates/hyperkitty/user_profile/last_views.html:46
#: templates/hyperkitty/user_profile/profile.html:16
#: templates/hyperkitty/user_profile/votes.html:47
msgid "Subject"
msgstr "Předmět"

#: templates/hyperkitty/user_profile/favorites.html:42
#: templates/hyperkitty/user_profile/last_views.html:47
#: templates/hyperkitty/user_profile/votes.html:48
msgid "Original author"
msgstr "Původní autor"

#: templates/hyperkitty/user_profile/favorites.html:43
#: templates/hyperkitty/user_profile/last_views.html:48
#: templates/hyperkitty/user_profile/votes.html:49
msgid "Start date"
msgstr "Datum zahájení"

#: templates/hyperkitty/user_profile/favorites.html:44
#: templates/hyperkitty/user_profile/last_views.html:49
msgid "Last activity"
msgstr "Poslední aktivita"

#: templates/hyperkitty/user_profile/favorites.html:71
msgid "No favorites yet."
msgstr "Nemáte zatím žádné oblíbené."

#: templates/hyperkitty/user_profile/last_views.html:22
#: templates/hyperkitty/user_profile/last_views.html:59
msgid "New comments"
msgstr "Nové komentáře"

#: templates/hyperkitty/user_profile/last_views.html:82
msgid "Nothing read yet."
msgstr "Nic nepřečteno."

#: templates/hyperkitty/user_profile/profile.html:9
msgid "Last posts"
msgstr "Poslední příspěvky"

#: templates/hyperkitty/user_profile/profile.html:17
msgid "Date"
msgstr "Datum"

#: templates/hyperkitty/user_profile/profile.html:19
msgid "Thread"
msgstr "Vlákno"

#: templates/hyperkitty/user_profile/profile.html:20
msgid "Last thread activity"
msgstr "Poslední aktivita ve vlákně"

#: templates/hyperkitty/user_profile/profile.html:49
msgid "No posts yet."
msgstr "Zatím žádné příspěvky."

#: templates/hyperkitty/user_profile/subscriptions.html:24
msgid "since first post"
msgstr "od prvního příspěvku"

#: templates/hyperkitty/user_profile/subscriptions.html:26
#: templates/hyperkitty/user_profile/subscriptions.html:63
msgid "post"
msgstr "příspěvek"

#: templates/hyperkitty/user_profile/subscriptions.html:31
#: templates/hyperkitty/user_profile/subscriptions.html:69
msgid "no post yet"
msgstr "zatím žádný příspěvek"

#: templates/hyperkitty/user_profile/subscriptions.html:42
msgid "Time since the first activity"
msgstr "Čas od první aktivity"

#: templates/hyperkitty/user_profile/subscriptions.html:43
msgid "First post"
msgstr "První příspěvek"

#: templates/hyperkitty/user_profile/subscriptions.html:44
msgid "Posts to this list"
msgstr "Příspěvky do tohoto listu"

#: templates/hyperkitty/user_profile/subscriptions.html:76
msgid "no subscriptions"
msgstr "žádné odběry"

#: templates/hyperkitty/user_profile/votes.html:32
#: templates/hyperkitty/user_profile/votes.html:70
msgid "You like it"
msgstr "Líbí se Vám"

#: templates/hyperkitty/user_profile/votes.html:34
#: templates/hyperkitty/user_profile/votes.html:72
msgid "You dislike it"
msgstr "Nelíbí se Vám"

#: templates/hyperkitty/user_profile/votes.html:50
msgid "Vote"
msgstr "Hlasovat"

#: templates/hyperkitty/user_profile/votes.html:83
msgid "No vote yet."
msgstr "Zatím žádné hlasy."

#: templates/hyperkitty/user_public_profile.html:7
msgid "User Profile"
msgstr "Uživatelský profil"

#: templates/hyperkitty/user_public_profile.html:14
msgid "User profile"
msgstr "Uživatelský profil"

#: templates/hyperkitty/user_public_profile.html:23
msgid "Name:"
msgstr "Jméno:"

#: templates/hyperkitty/user_public_profile.html:28
msgid "Creation:"
msgstr "Vytvořen:"

#: templates/hyperkitty/user_public_profile.html:33
msgid "Votes for this user:"
msgstr "Hlasy pro tohoto uživatele:"

#: templates/hyperkitty/user_public_profile.html:41
msgid "Email addresses:"
msgstr "Emailové adresy:"

#: views/message.py:75
msgid "This message in gzipped mbox format"
msgstr "Tato zpráva v gzipovaném formátu mbox"

#: views/message.py:201
msgid "Your reply has been sent and is being processed."
msgstr "Vaše odpověď byla odeslána a nyní se zpracovává."

#: views/message.py:205
msgid ""
"\n"
"  You have been subscribed to {} list."
msgstr ""
"\n"
"  Byli jste přihlášeni k listu {}."

#: views/message.py:288
#, python-format
msgid "Could not delete message %(msg_id_hash)s: %(error)s"
msgstr "Zprávu %(msg_id_hash)s se nepovedlo smazat: %(error)s"

#: views/message.py:297
#, python-format
msgid "Successfully deleted %(count)s messages."
msgstr "%(count)s zpráv bylo úspěšně smazáno."

#: views/mlist.py:88
msgid "for this month"
msgstr "pro tento měsíc"

#: views/mlist.py:91
msgid "for this day"
msgstr "pro tento den"

#: views/mlist.py:103
msgid "This month in gzipped mbox format"
msgstr "Tento měsíc v gzipovaném mbox formátu"

#: views/mlist.py:200 views/mlist.py:224
msgid "No discussions this month (yet)."
msgstr "V tomto měsíci (zatím) nebyly žádné diskuze."

#: views/mlist.py:212
msgid "No vote has been cast this month (yet)."
msgstr "V tomto měsíci neproběhlo žádné hlasování."

#: views/mlist.py:241
msgid "You have not flagged any discussions (yet)."
msgstr "Neovlaječkovali jste žádné diskuze."

#: views/mlist.py:264
msgid "You have not posted to this list (yet)."
msgstr "Do tohoto listu jste zatím nepřispěli."

#: views/mlist.py:357
msgid "You must be a staff member to delete a MailingList"
msgstr "Pro smazání mailing-listu musíte být jedním ze správců"

#: views/mlist.py:371
msgid "Successfully deleted {}"
msgstr "{} úspěšně smazáno"

#: views/search.py:115
#, python-format
msgid "Parsing error: %(error)s"
msgstr "Chyba parsování: %(error)s"

#: views/thread.py:169
msgid "This thread in gzipped mbox format"
msgstr "Toto vlákno v gzipovaném formátu mbox"

#~ msgid "days inactive"
#~ msgstr "dní neaktivní"

#~ msgid "days old"
#~ msgstr "dní starý"

#, python-format
#~ msgid ""
#~ "\n"
#~ "                    by %(name)s\n"
#~ "                    "
#~ msgstr ""
#~ "\n"
#~ "                    podle %(name)s\n"
#~ "                    "
